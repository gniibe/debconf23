# DebConf Video Training

---

# Roles

* Camera Operator
* Room Coordinator
* Director
* Sound Technician
* Talkmeister
* Video Core Team on call

---

# Camera Operator

* Two cameras per room
    * Speaker
    * Audience
* Cameras are always running
* Pan, Zoom
* Autofocus
* Tally Lights

---

# Room Coordinator

* The boss of the room
* Ensure everyone is there
* Ensures the presenter can hook up their laptop
    * Bag of adapters
* Introduces speaker to Audio Technician
* Ask for volunteers
* Fill in for anyone that is still missing

---

# Volunteers

* Arrive 5 minutes before the talk starts
* Check in with the Room Coordinator

---

# Director

* Operate the Live video mixer
    * Show us the speaker
    * Show the slides when they change
    * Give time to read slides
    * Keep the talk interesting
    * Avoid motion sickness
* Select A & B sources
* Composites: Fullscreen, Pic-in-pic, side-by-side
* Loopy
* Stream live vs stream blank
* Monitor and call for help on IRC

---

# Sound Technician

* Hardest job, unless you have audio experience
* Mixer: Vertical channels
    * Don't touch the knobs
* Generally aim for +0dB
* 2 headsets, 2 handhelds, laptop audio (stereo), 1 or 2 ambient
* Main mix: to the stream & jitsi (+10dB)
* Sub mix: to the PA. Set before adjusting main.
* Ambient doesn't go into the PA
* Sound check before the talk
* Monitor audio levels in voctomix

---

# Talkmeister

* Introduces the speaker
* Organizes questions
* Ensures the talk starts and ends on time.
* Talk to Nattie at Front Desk for training.

---

# Video Core Team on Call

* Sits in the Video Room
* Monitors IRC
* Responds to emergencies

---

# Volunteering

* Put your name & username down on the list
* Check the volunteer website
* Sign up!
* Remove yourself if you can't make it any more

---

# End of the Day

* Turn off audio gear
* Leave everything else running
* Attend the video team meeting
* `videoteam-copy-recordings`

---

# End of the conference

* We'll make teardown plans at the final meeting
* Don't unplug computers or network gear unless it's cleared by the core team
